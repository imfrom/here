---
layout: 
title: "Ssh Exchange Id Connection Closed by Host"
date: 2018-07-18T03:18:58-05:00
---
While updating some of the files through **Gitlab**, an
error came up on the screen with the message `ssh exchange
identification connection closed by host`.

According to <a href="https://unix.stackexchange.com/a/181132" target="_blank">this answers that says that by killing all sessions resolved it</a>. 

An answer that was linked to that same post recommended to
issue `
pkill -o -u YOURUSERNAME sshd` in order to kill the session. 

For further details about it see <a href="https://unix.stackexchange.com/a/127575" target="_blank">the accepted answer</a>


