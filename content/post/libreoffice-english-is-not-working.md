---
layout: 
title: "Libreoffice English Is Not Working"
date: 2018-06-13T02:41:50-05:00
---

The latest LibreOffice 6.0 somehow wasn't loading the
English dictionary as I thought it would. As a result the
hyphenation patterns is not being recognized.

It made little difference whether Tools -> Languages -> Language
Settings -> Languages -> English (COUNTRY) was selected.

I tried many of them, among them, from New Zealand, from US,
from UK, etcetera, and it would still made no discernible
difference on the outcome: automatic and manual
hyphenation was not returning any results.

I don't remember any of this from happening before. On the trivial
procedures as outlined on the post entitled [Libreoffice
Implementación del guión en la separación
silábica](/post/libreoffice-implementación-del-guión-en-la-separación-silábica)
in which I mainly went over some of the necessary steps during the
installation of the Spanish dictionary, I hadn't
struggled as much as I did this time, by trying to load the
English dictionary hyphenation features. I thought that I
was just going in opposite directions from what I was used, that is,  
whenever I've dealt with this problem in the past.

It seemed thought, that after many unsuccessful attempts,
the only solution left was to forcefully load the module
into the extension manager of the program.

But where would I find the so needed dictionary?

One of the most useless results from the Google search engine, turned out to be with the <a href="https://extensions.libreoffice.org/extensions/us-english-april-2011-word-list" target="_blank">US English - April 2011 word list</a> which failingly showed no available download links where I'd be able to at the very least check for testing drive the module.

In the case of the Spanish dictionary, I had never
encountered the same problems, as I was dealing with here
with the English hyphenation. 

I checked again and again the steps to be load it
automatically through the styles of the document, and also
by doing it manually, but to no avail.

This time around I downloaded and consequently opened the <a
href="https://extensions.libreoffice.org/extensions/diccionario-espanol" target="_blank">diccionario español</a> and proceeded to load it right after with the LibreOffice Extension manager.

What could have been the problem with the incorrect handling of
hyphenation patterns with the Libreoffice system that for so long has worked without a major setback?  The file <a href="https://extensions.libreoffice.org/extensions/spanish-dictionaries" target = "_blank">es_ANY.oxt seemed to work without a problem.</a> Something else must have caused it to load it incorrectly.

I went ahead and installed the <a href="https://extensions.libreoffice.org/extensions/english-dictionaries" target="_blank">English Dictionaries</a>, which then threw an error/warning advising that the module was already loaded. And it also prompted if that module recently used, ought to replace the default one. To what I agreed.

After LibreOffice restarted I was surprised - considering
that there was only a user (not me) who found it noteworthy
- that it seemed to work without a major problem.

The project - at the time of this writing - holds an Apache
License and it specifies that most of the icons are from
OpenOffice, and it's trademarked Proofing Tool GUI.

More information about it can be seen at: http://marcoagpinto.cidadevirtual.pt/proofingtoolgui.html

On the other hand, after disabling the above extension, I
retried to enable once again the <a href="https://extensions.libreoffice.org/extensions/american-british-canadian-spelling-hyphen-thesaurus-dictionaries">American British Canadian - spelling/hyphen/thesaurus dictionaries</a> to which my amazement seemed to work without a glitch this time.

The project page is at http://libreoffice-na.us/ for more
information. 

It could be a bug with the recent release of LibreOffice,
but I just don't have enough details to reproduce the
problem. It seemed that the dictionary files needed to be
reinstalled to get it working accordingly.



